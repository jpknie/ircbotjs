// author Jani Nieminen, jpkniem@gmail.com 2020

let NodeIRC = require('node-irc');
let schedule = require('node-schedule');
let https = require('https');
let nick = "noita-bot";
let chan = "#80386";
let client = new NodeIRC('irc.cc.tut.fi', 6667, nick, 'noita-bot');

let coronaUrl = 'https://w3qa5ydb4l.execute-api.eu-west-1.amazonaws.com/prod/finnishCoronaData';

let coronaData = {};

let districts = ['Etelä-Karjala',
                'Etelä-Pohjanmaa',
                'Etelä-Savo',
                'HUS',
                'Itä-Savo',
                'Kainuu',
                'Kanta-Häme',
                'Keski-Pohjanmaa',
                'Keski-Suomi',
                'Kymenlaakso',
                'Lappi',
                'Länsi-Pohja',
                'Pirkanmaa',
                'Pohjois-Karjala',
                'Pohjois-Pohjanmaa',
                'Pohjois-Savo',
                'Päijät-Häme',
                'Satakunta',
                'Vaasa',
                'Varsinais-Suomi'];


function __getCoronaData() {
  return new Promise(resolve => {
      https.get(coronaUrl, (response) => {
      var body = '';
      response.on('data', (chunk) => {
        body += chunk;
      });
      response.on('end', () => {
        coronaData = JSON.parse(body);
        resolve();
      });
    }).on('error', (e) => {
      console.log(e);
    });
  });
}

client.verbosity = 2;

client.on('CHANMSG', (data) => {
  if(data.message[0] == '!') {
    let commandAndParams = data.message.split("!")[1].split(' ');
    let command = commandAndParams[0];
    let param1 = commandAndParams[1];

    let func = messages[command];
    if(func !== undefined && func != null)
      func(data.receiver, data.sender, param1);
  }
});

let getCoffee = (receiver, nickname, param) => {
  client.say(receiver, "Here's nice cup of coffee cŨ for you " + nickname);
}

let sayCountsByDistrict = (receiver, district) => {
    if(districts.indexOf(district) == -1) {
      client.say(receiver, "District not found.");
    } else {
      let countAtDistrict = coronaData.confirmed.filter((element) => element.healthCareDistrict === district).length;
      client.say(receiver, "Confirmed infections at " + district + " is now " + countAtDistrict);
    }
    client.say(receiver, "Confirmed infections (total) " + coronaData.confirmed.length);
    client.say(receiver, "Recoveries (total) " + coronaData.recovered.length);
    client.say(receiver, "Deaths (total) " + coronaData.deaths.length);
}

async function __getCoronaDataAndSayCounts() {
  await __getCoronaData();
  sayCountsByDistrict(chan, "HUS");
}

schedule.scheduleJob('0 0 * * *', () => {
    __getCoronaDataAndSayCounts()
}); // run everyday at midnight

let getCoronaData = (receiver, nickname, district) => {
  if(coronaData == null || coronaData === undefined) {
    client.say(receiver, "No corona data yet available");
  }
  else {
    sayCountsByDistrict(receiver, district);
  }
}

client.on('ready', () => {
  client.join(chan);
  __getCoronaData();
});

let messages = {
  'corona': getCoronaData,
  'coffee': getCoffee
}

client.connect();
